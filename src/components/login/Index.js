import React, { useState, useCallback, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import axios from 'axios'
import './style.css'

export default function Login () {
  const navigate = useNavigate()
  const [userData, setUserData] = useState([])

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/'
  })

  useEffect(() => {
    Api.get('crudData')
      .then((res) => {
        setUserData(res.data)
        console.log('res.data', res.data)
      })
      .catch((err) => {
        console.log('err', err)
      })
  }, [])

  const [userLoginData, setUserLoginData] = useState({
    uEmail: '',
    uPassword: ''
  })

  const [loginDetails, setLoginDetails] = useState([])
  const handleChange = (e) => {
    const { name, value } = e.target

    setUserLoginData({ ...userLoginData, [name]: value })
  }
  // console.log('userLoginData', userLoginData)

  const handleSubmitButton = useCallback(
    (e) => {
      e.preventDefault()
      const newDetails = { ...userLoginData }

      setLoginDetails([...loginDetails, newDetails])
      setUserLoginData(userLoginData)

      if (userData === null) {
        alert('User does not Exist.')
      } else {
        const storeLoginData = userData.find((element) =>
          element.uEmail === userLoginData.uEmail
        )
        if (storeLoginData.uPassword === userLoginData.uPassword) {
          sessionStorage.setItem('email', storeLoginData.uEmail)
          sessionStorage.setItem('password', storeLoginData.uPassword)
          navigate('/dashBoard')
        } else {
          alert('Invalid Email or Passwrord.')
        }
      }
    },
    [userLoginData, setLoginDetails]
  )

  return (
    <div className='container'>
      <form>
        <h1>Login Page</h1>
        <div className='input'>
            <label>Username</label>
            <input type='email' value={userLoginData.uEmail} name='uEmail' onChange={(e) => handleChange(e)} />
        </div>
        <div className='input'>
            <label>Password</label>
            <input type='password' value={userLoginData.uPassword} name='uPassword' onChange={(e) => handleChange(e)} />
        </div>
        <div className='input'>
            <button type='submit' onClick={(e) => handleSubmitButton(e)}>Login</button>
        </div>
        <div className='input'>
            <p>Don&apos;t  have an Account? <Link to='/'>SignUp</Link></p>
        </div>
      </form>
    </div>
  )
}
