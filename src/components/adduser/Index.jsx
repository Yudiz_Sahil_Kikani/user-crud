import { React, useState } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import Navbar from '../navbar'
import './style.css'

function Adduser () {
  const [uFirstname, setFirstname] = useState('')
  const [uLastname, setLastname] = useState('')
  const [uEmail, setEmailid] = useState('')
  const [uJobtitle, setJobtitle] = useState('')
  const [uGender, setGender] = useState('')
  const [uCountry, setCountry] = useState('')
  const [uAddress, setAddress] = useState('')
  const [uMobilenumber, setMobilenumber] = useState('')
  const navigate = useNavigate()

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/'
  })

  // console.log(uFirstname)
  // console.log(ulastname)
  // console.log(uEmail)
  // console.log(uPassword)

  const addDatatoapi = (e) => {
    Api.post('crudData', {
      uFirstname,
      uLastname,
      uEmail,
      uJobtitle,
      uGender,
      uCountry,
      uAddress,
      uMobilenumber
    })
      .then(() => {
        navigate('/users')
      })
    e.preventDefault()
  }

  return (
    <>
    <Navbar/>
<div className='registration'>
      <h2>Add user</h2>
  <form className='adduser'>
    <div className='row'>
      <label>First name</label>
      <input
        id='input'
        required
        name = 'uFirstname'
        placeholder='First name'
        onChange ={(e) => setFirstname(e.target.value)}
      /><br/>
      <label>Last name</label>
      <input
        className='input'
        required
        name='uLastname'
        placeholder='Last name'
        onChange ={(e) => setLastname(e.target.value)}
      /><br/>

    </div>
    <label>Email-id</label>
    <input
      className='input'
      required
      name = 'uEmail'
      placeholder='joe@schmoe.com'
      onChange ={(e) => setEmailid(e.target.value)}
    /><br/>
    <label>Job title</label>
     <input
      className='input'
      required
      name = 'uJobtitle'
      type='text'
      placeholder='jobtitle'
      onChange ={(e) => setJobtitle(e.target.value)}
    /><br/>
    <label>Gender</label>
     <input
      className='input'
      required
      name = 'uGender'
      type='text'
      placeholder='Gender'
      onChange ={(e) => setGender(e.target.value)}
    /><br/>
    <label>Country</label>
     <input
      className='input'
      required
      name = 'uCountry'
      type='text'
      placeholder='Country'
      onChange ={(e) => setCountry(e.target.value)}
    /><br/>
    <label>Address</label>
     <input
      className='input'
      required
      name = 'uAddress'
      type='text'
      placeholder='Address'
      onChange ={(e) => setAddress(e.target.value)}
    /><br/>
    <label>Mobile number</label>
     <input
      className='input'
      required
      name = 'uMobilenumber'
      type='text'
      placeholder='Mobile number'
      onChange ={(e) => setMobilenumber(e.target.value)}
    /><br/>
    <div>
    <button
    onClick={addDatatoapi}
    >
        Add user
        </button>

    </div>
  </form>

</div>
</>
  )
}

export default Adduser
