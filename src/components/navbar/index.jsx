import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

function Navbar () {
  return (
    <div className='navbar'>
      <nav>
        <ul>
          <Link to="/dashboard">
            <li>
              <a>Home</a>
            </li>
          </Link>
          <Link to="/users">
            <li>
              <a>User</a>
            </li>
          </Link>
        </ul>
      </nav>
    </div>
  )
}

export default Navbar
