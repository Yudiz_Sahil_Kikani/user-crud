import React, { useEffect, useState } from 'react'
import { Table, Button } from 'semantic-ui-react'
import axios from 'axios'
import { Link, useNavigate } from 'react-router-dom'
import './style.css'
import Navbar from '../navbar/index'

export default function Users () {
  const navigate = useNavigate()
  const [apiData, setApiData] = useState([])
  const [order, setOrder] = useState('ASC')
  const [search, setSearch] = useState('')
  const [sortData, setSortData] = useState('all')
  const [inputText, setInputText] = useState('')

  console.log(apiData)

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/crudData/'
  })

  useEffect(() => {
    Api.get('')
      .then((getData) => {
        setApiData(getData.data)
      })
  }, [])

  const getData = () => {
    Api.get('')
      .then((getData) => {
        setApiData(getData.data)
      })
  }

  const onDelete = (id) => {
    console.log(id)
    Api.delete(`${id}`)
      .then((res) =>
        console.log('delete data', res.data)
      )
      .then(() => {
        getData()
      })
      .catch((error) => {
        console.log(error)
      })
  }

  const inputHandler = (e) => {
    setSearch('')
    const lowerCase = e.target.value.toLowerCase()
    setInputText(lowerCase)
  }
  const newdata = apiData.filter((object) => {
    return object.uFirstname.toLowerCase().includes(search)
  })
  const check = () => {
    setSearch(inputText)
  }

  const sorting = (col) => {
    if (order === 'ASC') {
      const sorted = [...apiData].sort((a, b) =>
        a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
      )
      setApiData(sorted)
      setOrder('DSC')
    }
    if (order === 'DSC') {
      const sorted = [...apiData].sort((a, b) =>
        a[col].toLowerCase() < b[col].toLowerCase() ? 1 : -1
      )
      setApiData(sorted)
      setOrder('ASC')
    }
  }
  useEffect(() => {
    if (sortData === true) {
      const filter = (data) => data.filter((item) => item.uGender === 'm')
      setApiData(filter)
    } else if (sortData === false) {
      const filter = (data) => data.filter((item) => item.uGender === 'f')
      setApiData(filter)
    } else {
      getData()
    }
  }, [sortData])

  const handleFilter = (e) => {
    const { value } = e.target
    console.log('value :>> ', value);
    (value === 'true') ? setSortData(true) : value === 'false' ? setSortData(false) : setSortData('all')
  }

  return (
    <div>
          <Navbar/>
           <Link to='/dashboard'>
           <Button color="blue">Go Back</Button>
           </Link>

           <Link to='/users/adduser'>
           <Button color="red">Add user</Button>
           </Link><br/>

           <Button color="green"
              onClick={() => sorting('uFirstname')}
            >Sort</Button>
            <div>
            <select value={sortData} onChange={(e) => handleFilter(e)}>
      <option value={'all'}>All</option>
      <option value={true}>Male</option>
      <option value={false}>Female</option>
    </select>
            </div><br/>

           <div>

            <input
              type="text"
            placeholder="Searh..."
            onChange={inputHandler}
            />
            <button onClick={check}>Search</button>
  </div>

            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>First Name</Table.HeaderCell>
                        <Table.HeaderCell>Last Name</Table.HeaderCell>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>Jobtitle</Table.HeaderCell>
                        <Table.HeaderCell>Gender</Table.HeaderCell>
                        <Table.HeaderCell>Profilepic</Table.HeaderCell>
                        <Table.HeaderCell>country</Table.HeaderCell>
                        <Table.HeaderCell>Address</Table.HeaderCell>
                        <Table.HeaderCell>Bio</Table.HeaderCell>
                        <Table.HeaderCell>Mobile number</Table.HeaderCell>
                        <Table.HeaderCell>status</Table.HeaderCell>
                        <Table.HeaderCell>Update</Table.HeaderCell>
                        <Table.HeaderCell>Delete</Table.HeaderCell>

                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {newdata.map((data, index) => {
                      return (
                            <Table.Row key={index}>
                                <Table.Cell>{data.id}</Table.Cell>
                                <Table.Cell>{data.uFirstname}</Table.Cell>
                                <Table.Cell>{data.uLastname}</Table.Cell>
                                <Table.Cell>{data.uEmail}</Table.Cell>
                                <Table.Cell>{data.uJobtitle}</Table.Cell>
                                <Table.Cell>{data.uGender}</Table.Cell>
                                <Table.Cell><img src={data.uProfilepic}/></Table.Cell>
                                <Table.Cell>{data.uCountry}</Table.Cell>
                                <Table.Cell>{data.uAddress}</Table.Cell>
                                <Table.Cell>{data.uBio}</Table.Cell>
                                <Table.Cell>{data.uMobilenumber}</Table.Cell>
                                <Table.Cell>{data.uStatus}</Table.Cell>

                               <Table.Cell>
                                <Button color="black"
                                onClick={() => navigate('/users/update', { state: { data } })}
                                 >Update</Button>

                                 </Table.Cell>
                                 <Table.Cell>
                                <Link to='/users'>
                                    <Button color="red"
                                    onClick={() => onDelete(data.id)}
                                    >Delete</Button>
                                    </Link>
                                </Table.Cell>
                            </Table.Row>
                      )
                    })}

                </Table.Body>
            </Table>
        </div>
  )
}
