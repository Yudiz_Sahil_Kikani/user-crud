import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import axios from 'axios'
import Navbar from '../navbar'

export default function Update () {
  const naviagte = useNavigate()
  const { state } = useLocation()
  // console.log(state.data.id)
  const id = state.data.id
  const [uFirstname, setFirstName] = useState(state.data.uFirstname)
  const [uLastname, setLastName] = useState(state.data.uLastname)
  const [uEmail, setEmailID] = useState(state.data.uEmail)
  const [uJobtitle, setJobTitle] = useState(state.data.uJobtitle)
  const [uGender, setGender] = useState(state.data.uGender)
  const [uCountry, setCountry] = useState(state.data.uCountry)
  const [uAddress, setAddress] = useState(state.data.uAddress)
  const [uMobilenumber, setMobileNumber] = useState(state.data.uMobilenumber)
  // console.log(state.data.uFirstname)
  const handelupdate = (e) => {
    // console.log(id)
    axios.put(`https://6364b00c8a3337d9a2fc18f2.mockapi.io/crudData/${id}`, {
      uFirstname, uLastname, uEmail, uJobtitle, uGender, uCountry, uAddress, uMobilenumber
    })
      .then((res) => {
        console.log('res.data', res.data)
      })
      .then(() => naviagte('/users'))
  }
  return (
    <div>
      <Navbar/>
      <div className='registration'>
            <h2>Update user</h2>
        <form className='adduser'>
          <div className='row'>
            <label>First name</label>
            <input
              id='uFirstname'
              required
              value={uFirstname}
              name = 'uFirstname'
              placeholder='First name'
              onChange ={(e) => setFirstName(e.target.value)}
            /><br/>
            <label>Last name</label>
            <input
              className='input'
              required
              value={uLastname}
              name='uLastname'
              placeholder='Last name'
              onChange ={(e) => setLastName(e.target.value)}
            /><br/>

          </div>
          <label>Email-id</label>
          <input
            className='input'
            required
            name = 'uEmail'
            value={uEmail}
            placeholder='joe@schmoe.com'
            onChange ={(e) => setEmailID(e.target.value)}
          /><br/>
          <label>Job title</label>
           <input
            className='input'
            required
            name = 'uJobtitle'
            type='text'
            value={uJobtitle}
            placeholder='jobtitle'
            onChange ={(e) => setJobTitle(e.target.value)}
          /><br/>
          <label>Gender</label>
           <input
            className='input'
            required
            name = 'uGender'
            type='text'
            value={uGender}
            placeholder='Gender'
            onChange ={(e) => setGender(e.target.value)}
          /><br/>
          <label>Country</label>
           <input
            className='input'
            required
            name = 'uCountry'
            type='text'
            placeholder='Country'
            value={uCountry}
            onChange ={(e) => setCountry(e.target.value)}
          /><br/>
          <label>Address</label>
           <input
            className='input'
            required
            name = 'uAddress'
            type='text'
            value={uAddress}
            placeholder='Address'
            onChange ={(e) => setAddress(e.target.value)}
          /><br/>
          <label>Mobile number</label>
           <input
            className='input'
            required
            name = 'uNumber'
            type='text'
            value={uMobilenumber}
            placeholder='Mobile number'
            onChange ={(e) => setMobileNumber(e.target.value)}
          /><br/>
          <div>

          </div>
        </form>
                <button
          onClick={handelupdate}
          >
              Update
              </button>
        </div>
        </div>
  )
}
