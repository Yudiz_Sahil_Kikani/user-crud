import { React, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'

function Protected (props) {
  const { Component } = props
  const navigate = useNavigate()

  useEffect(() => {
    const password = sessionStorage.getItem('password')
    if (!password) { navigate('/login') }
  })

  return (
    <div>
        <Component/>
    </div>
  )
}

Protected.propTypes = {
  Component: PropTypes.func.isRequired
}

export default Protected
