import { React, useState } from 'react'
import axios from 'axios'
import { useNavigate, Link } from 'react-router-dom'
import './Style.css'

function Registration () {
  const [uFirstname, setFirstname] = useState('')
  const [uLastname, setLastname] = useState('')
  const [uEmail, setEmailid] = useState('')
  const [uPassword, setpassword] = useState('')
  const navigate = useNavigate()

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/'
  })

  // console.log(uFirstname)
  // console.log(ulastname)
  // console.log(uEmail)
  // console.log(uPassword)

  const addDatatoapi = (e) => {
    Api.post('crudData', {
      uFirstname,
      uLastname,
      uEmail,
      uPassword
    })
      .then(() => {
        navigate('/login')
      })
    e.preventDefault()
  }

  return (
<div className='registration'>
      <h2>Registration Form</h2>
  <form>
    <div className='row'>
      <label>First name</label>
      <input
        id='input'
        required
        name = 'fname'
        placeholder='First name'
        onChange ={(e) => setFirstname(e.target.value)}
      /><br/>
      <label>Last name</label>
      <input
        className='input'
        required
        name='lname'
        placeholder='Last name'
        onChange ={(e) => setLastname(e.target.value)}
      /><br/>

    </div>
    <label>Email-id</label>
    <input
      className='input'
      required
      name = 'email'
      placeholder='joe@schmoe.com'
      onChange ={(e) => setEmailid(e.target.value)}
    /><br/>
    <label>Password</label>
     <input
      className='input'
      required
      name = 'password'
      type='password'
      placeholder='*******'
      onChange ={(e) => setpassword(e.target.value)}
    /><br/>
    <div>
    <button
    onClick={addDatatoapi}
    >
        Registration
        </button>
        <p>You have an Account? <Link to='/login'>Login</Link></p>

    </div>
  </form>

</div>
  )
}

export default Registration
