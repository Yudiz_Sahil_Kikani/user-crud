import React, { useEffect, useState } from 'react'
import Navbar from '../navbar/index'
import axios from 'axios'

function DashBoard () {
  const [apiData, setApiData] = useState([])

  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/crudData/'
  })

  useEffect(() => {
    Api.get('')
      .then((getData) => {
        setApiData(getData.data)
      })
  }, [])

  const totaluser = apiData.length
  const maleuser = apiData.filter((item) => item.uGender === 'm')
  const Male = maleuser.length
  const femaleuser = apiData.filter((item) => item.uGender === 'f')
  const Female = femaleuser.length

  return (
    <>
    <Navbar/>
    <div>
      <h2>Total user :- <span>{totaluser}</span></h2>
      <h2>Male user :- <span>{Male}</span></h2>
      <h2>Female user :- <span>{Female}</span></h2>
    </div>
    </>
  )
}

export default DashBoard
