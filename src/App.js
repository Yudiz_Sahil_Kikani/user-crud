/* eslint-disable no-unused-vars */
import React from 'react'
import './App.css'
import Registration from './components/registration/Index'
import Login from './components/login/Index'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import DashBoard from './components/dashboard/index'
import Users from './components/user/index'
import Update from './components/update/Index'
import Adduser from './components/adduser/Index'
import Protected from './components/protected/protected'

function App () {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<Registration/>}/>
          <Route path='/login' element={<Login />}/>
          <Route path='/dashboard' element={<Protected Component={DashBoard}/>}/>
          <Route path="/users" >
         <Route index element={<Protected Component={Users}/> } />
         <Route path='adduser' element={<Protected Component={Adduser}/>}/>
         <Route path='update' element={<Protected Component={Update} />} />
       </Route>
       {/* <Route path="*" element={<Notfound />} /> */}
        </Routes>
      </Router>
    </div>
  )
}

export default App
